var odex = require('odex');
s = new odex.Solver(2);
var LotkaVolterra = function (a, b, c, d) {
    return function (x, y) {
        return [
            a * y[0] - b * y[0] * y[1],
            c * y[0] * y[1] - d * y[1]
        ];
    };
};




function calculateLotkaVolterra(alpha, beta, delta, gamma, timeSeries, initialPopulation){
    predatorPopulations = [];
    preyPopulations = [];
    var preyInitialPopulation = parseInt(initialPopulation[0]);
    var predatorInitialPopulation = parseInt(initialPopulation[1]);
    var times = [];
    for(var i = 0; i < timeSeries; i++){
        times.push(i);
        var val = s.solve(LotkaVolterra(alpha, beta, delta, gamma), 0, [preyInitialPopulation, predatorInitialPopulation], i).y;
        preyPopulations.push(val[0]);
        predatorPopulations.push(val[1]);
    }

    return {
        times : times,
        preyPopulations : preyPopulations,
        predatorPopulations : predatorPopulations
    };
}


var tickChart;
var counter = 0;
var timeMax = 0;

function updateChartTick(k1, k2, k3, k4, initialPopulation) {
    if (counter == timeMax) {
        clearInterval(tickChart);
        counter = 0;
    } else {
        var config = calculateLotkaVolterra(k1, k2, k3, k4, counter, initialPopulation);

        window.myLine.data.labels = config.times;
        window.myLine.data.datasets[0].data = config.preyPopulations;
        window.myLine.data.datasets[1].data = config.predatorPopulations;
        window.myLine.update();
    }
}

var config = {
    type: 'line',
    data: {
        labels: 0,
        datasets: [{
            label: 'Prey',
            backgroundColor: '#1e90ff',
            borderColor: '#1e90ff',
            data: 0,
            fill: false,
        }, {
            label: 'Predator',
            fill: false,
            backgroundColor: '#ff6b81',
            borderColor: '#ff6b81',
            data: 0,
        }]
    },
    options: {
        elements: {
            point: {
                radius: 0
            }
        },
        responsive: true,
        title: {
            display: true,
            text: 'Lotka Volterra Model'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Population'
                }
            }]
        }
    }
};



$('.calculate-lotka-volterra').click(function () {
    clearInterval(tickChart);

    var k1 = $('.k1-text').val();
    var k2 = $('.k2-text').val();
    var k3 = $('.k3-text').val();
    var k4 = $('.k4-text').val();
    var time = $('.time-text').val();
    var initialPopulation = [$('.prey0-text').val(), $('.predator0-text').val()]
    timeMax = time;
    
    //console.log(initialPopulation);
    tickChart = setInterval(function () {
        counter = counter + 1;
        updateChartTick(k1, k2, k3, k4, initialPopulation);
    }, 10);


});

window.onload = function () {	
    var ctx = document.getElementById('myChart').getContext('2d');
    window.myLine = new Chart(ctx, config);
};

